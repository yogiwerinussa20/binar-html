const itemStep = document.querySelectorAll(".accordion-item");
const steps = document.querySelector(".steps");

itemStep.forEach((step) => {
  step.addEventListener("click", () => {
    const lists = steps.children;
    for (list of lists) {
      list.classList.remove("active-steps");
    }
    step.classList.add("active-steps");
  });
});
